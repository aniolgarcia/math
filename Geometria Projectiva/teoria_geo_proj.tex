\documentclass[utf8, catalan]{report}
\usepackage[utf8]{inputenc}
\usepackage[catalan]{babel} %install texlive-lang-spanish
\usepackage{listings}
\usepackage{amstext}
\usepackage{authblk}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{color} 
\usepackage{enumerate}
\usepackage{faktor} %found in texlive-science
\usepackage{ stmaryrd }
\usepackage{ mathrsfs }
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage[nottoc]{tocbibind}

\newenvironment{demo}[1][\proofname]{%
  \begin{proof}[#1]$ $\par\nobreak\ignorespaces
}{%
  \end{proof}
}

\newtheoremstyle{break}% name
  {}%         Space above, empty = `usual value'
  {}%         Space below
  {\itshape}% Body font
  {}%         Indent amount (empty = no indent, \parindent = para indent)
  {\bfseries}% Thm head font
  {}%        Punctuation after thm head
  {\newline}% Space after thm head: \newline = linebreak
  {}%
\theoremstyle{break}

\newtheorem{defi}{Definició}
\newtheorem{obs}{Observació}
\newtheorem{prop}{Proposició}
\newtheorem{ex}{Exemple}
\newtheorem*{thm}{Teorema}
\newtheorem{lema}{Lema}
\newtheorem{coro}{Corol·lari}

%\newcommand*\quot[2]{{^{\textstyle #1}\big/_{\textstyle #2}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}
%
\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\makeatother

\title{Geometria projectiva}
\author{Aniol Garcia i Serrano}
\date{}

\begin{document}

\maketitle
\section{Intro}
\begin{defi}
Un espai projectiu sobre un cos $k$ de dimensió $n$ és una terna formada per:
\begin{enumerate}
    \item Un conjunt $\mathbb{P}$ (els elements del qual s'anomenen punts)
    \item Un espai vectorial $E$ sobre $k$ de dimensió $n+1$
    \item Una aplicació exhaustiva $\pi\colon E\setminus \{0\}\to \mathbb{P}$ tal que $\pi(v)=\pi(w)\iff v=\lambda w$, per a alguna $\lambda \in k$.
\end{enumerate}
A un espai projectiu $(\mathbb{P}, E, \pi)$, l'anomenarem comunament $\mathbb{P}$. $\pi(v)=p \overset{\text{notació}}{\iff} p=[v]$ i diem que $p$ és representat per $v$ o que $v$ representa a $p$. 
\end{defi}

\begin{defi}
$L\subset \mathbb{P}$ s'anomena varietat lineal $\iff L=\pi(F\setminus \{0\}) := [F]$, anb $F\subset E$ un subespai.
\end{defi}

\begin{prop}
Si $L=[F]$, aleshores $F\setminus\{0\}=\pi^{-1}(L)$ i, per tant, $L$ determina $F$.
\end{prop}
\begin{demo}
\begin{itemize}
    \item[$\boxed{\subset}$] $v\in F\setminus\{0\}\overset{L=\pi(F\setminus\{0\})}{\implies} \pi(v)\in L \iff v\in\pi^{-1}(L)$.
    \item[$\boxed{\supset}$] $v\in \pi^{-1}(L)\iff \pi(v)\in L\iff \exists w\in F \| \pi(w)=\pi(v) \implies v=\lambda w \implies v\in F$.
\end{itemize}
\end{demo}

\begin{defi}
$dim\, L = dim\, F-1$
\end{defi}

\begin{obs}
L'aplicació 
\begin{align*}
     & Sub_{d+1}(E)\longrightarrow V.Lin_{d}(\mathbb{P}_n) \\
     & F \longmapsto [F]
\end{align*}
és bijectiva per definició de varietat lineal i la proposició anterior.
\end{obs}

\begin{obs}
Si $L=[F]$ varietat lineal de dimensió $d\geq 1$, aleshores $(L, F, \pi_{|F-\{0\}})$ és espai projectiu de dimensió $d$.
\end{obs}

\subsection{Incidència de varietats lineals}
\textbf{En tota aquesta secció suposarem que $L_1= [F_1]$ i que $L_2=[F_2]$}

\begin{prop}
$L_1\subset L_2 \iff F_1\subset F_2$
\end{prop}
\begin{demo}
\begin{itemize}
    \item[$\boxed{\implies}$]: Agafem $v\in F_1$. Si $v=0$, aleshores $v\in F_2$. Si $v\neq 0$, aleshores $[v]\in L_1$, $[v]\in L_2 \implies v\in F_2$.
    \item[$\boxed{\impliedby}$]: $F_1\subset F_2 \implies F_1\setminus \{0\}\subset F_2\setminus \{0\}$, aleshores $L_1=\pi(F_1\setminus \{0\})\subset \pi(F_2\setminus \{0\})= L_2$
\end{itemize}
\end{demo}

\begin{prop}
$L_1\cap L_2 = [F_1 \cap F_2]$
\end{prop}
\begin{demo}
Volem veure que $L_1\cap L_2 = \pi{(F_1\cap F_2 )\setminus \{0\}}$
\begin{itemize}
    \item[$\boxed{\implies}$]: $p\in L_1\cap L_2 \implies \begin{cases} p\in L_1 \implies p = [v],\, v\in F_1
    \\ p\in L_2 \implies p = [w],\, w\in F_2
    \end{cases} \implies \lambda v= w \implies p=[v]=[w] \implies w\in F_1 \implies w\in F_1 \cap F_2 \implies p\in [F_1 \cap F_2]$

    \item[$\boxed{\impliedby}$]: $p \in \pi((F_1\cap F_2)\setminus \{0\}) \implies p = [v],\; v\in F_1 \cap F_2 \implies \begin{cases}p=[v], v\in F_1 \implies p\in L_1 \\ p=[v], v\in F_2\implies p\in L_2 \end{cases} \implies p\in L_1\cap L_2$
\end{itemize}
\end{demo}

\begin{prop}$L_1\cap L_2 \subset L_i,\; i=1,2$.  Si $T$ varietat lineal compleix $T\subset L_1$, aleshores $T\subset L_1\cap L_2$.
\end{prop}

\begin{defi}
$L_1 \vee L_2 = [F_1 + F_2] = $ unció de (suma de, varietat geenrada per $L_1$ i $L_2$.
\end{defi}

\begin{prop}
$L_1 \vee L_2 \supset L_i$, $i=1,2$ i si $T$ varietat lineal compleix que $T\supset L_1$ i $T\supset L_2$, aleshores $T\supset L_1 \cup L_2$.
\end{prop}
\begin{demo}
Per àlgebra lineal, $F_1 + F_2 \supset F_1 \implies L1 \vee L_2 = [F_1 + F_2] \supset [F_1] = L_1$. Ídem per $F_2$ i $L_2$. $T=[F]$ i $T\supset L_i \implies F\supset L_i \implies F\supset F_1 + F_2$. Aleshores $T=[F]\supset [F_1 + F_2] = L_1 \vee L_2$.
\end{demo}

\begin{prop}
\begin{enumerate}
    \item $L_1 \subset L_1 \implies dimL_1 \leq dim L_2$
    \item $\begin{cases} L_1 \subset L_2 \\ dim L_1 = dim L_2 \end{cases} \implies L_1 = L_2$
\end{enumerate}
\end{prop}
\begin{demo}
\begin{enumerate}
    \item $L_1 \subset L_2 \implies F_1 \subset F_2 \implies dim F_1 \leq dim F_2 \implies dim L_1 \leq dim L_2$
    \item $\begin{cases} L_1 \subset L_2 \\ dim L_1 = dim L_2 \end{cases} \implies \begin{cases}F_1\subset F_2 \\ dim F_1 = dim F_2\end{cases} \implies F_1 = F_2 \implies L_1 = L_2$
\end{enumerate}
\end{demo}

\begin{thm}
$dim L_1 + dim L_2 = dim L_1\cap L_2 + dim L_1 \vee L_2$
\end{thm}
\begin{demo}
Representant les dimensions de les varietats lineals en funció dels seus espais vectorials associats tenim: $dim F_1 -1 + dim F_2 -1 = dim (F_1 \cap F_2 ) -1 + dim (F_1+ F_2) -1$, que és la fórmula de Grassman que coneixem de l'àlgebra lineal.
\end{demo}

\begin{prop}
$\begin{cases}L_1 = [F_1] \subset L_1'=[F_1'] \\ L_2 = [F_2] \subset L_2'=[F_2']\end{cases} \implies L_1 \vee L_2 \subset L_1' \vee L_2'$
\end{prop}
\begin{demo}
$\begin{cases}L_1\subset L_1' \subset L_1'\vee L_2' \\ L_2 \subset L_2' \subset L_1' \vee L_2'\end{cases} \implies L_1\vee L_2 \subset L_1'\vee L_2'$
\end{demo}

\begin{lema}
Sigui $L$ una varietat lineal. Si $p\not\in L$, $dim L\vee\{p\} = dim L +1$
\end{lema}
\begin{demo}
Per Grassman tenim que
$dim L + dim \{p\} = dim L \vee \{p\} + dim L\cap \{p\} \implies dim L + 0 = dim L\vee\{p\} -1$
\end{demo}

\begin{lema}
$H$ hiperplà, $L$ varietat lineal tal que $H\not\supset L$. aleshores $dim L\cap H = dim L -1$.
\end{lema}
\begin{demo}
Per Grassman tenim que $dim L + n-1 = dim L\cap H + dim L\vee H$. Però $L\vee H$ només pot ser el total, ja que per hipòtesi $L$ no està continguda a l'hiperplà. Així ens queda $dim L + n-1 = dim L\cap H+ n$.
\end{demo}

\begin{defi}
$L_1$, $L_2$ viaretats lineals de $\mathbb{P}_n$ es diuen suplementàries $\iff \begin{cases}L_1\cap L_1 = \O \\ L_1 \vee L_2 =\mathbb{P}_n\end{cases} \iff \begin{cases}F_1\cap F_2 = \{0\} \\ F_1 + F_2 = E\end{cases}\iff E = F_1 \oplus F_2$
\end{defi}

\begin{lema}
Dues condicions qualssevol entre les següents
\begin{enumerate}
    \item $L_1\cap L_2 = \O$
    \item $L_1\vee L_2 = \mathbb{P}_n$
    \item $dim(L_1)+dim(L_2)= n-1$
\end{enumerate}
impliquen la tercera.
\end{lema}

\begin{lema}
Donats els punts $q_0, q_1, \dotsc, q_n\in \mathbb{P}_n$:
\begin{enumerate}
    \item $dim (q_0\vee q_1\vee \dotsc \vee q_m) \leq m$
    \item $dim(q_0\vee q_1\vee \dotsc\vee q_m) = m \iff \forall i >0, q_i \not\in (q_0\vee q_1\vee \dotsc\vee q_{i-1})$
\end{enumerate}
\end{lema}
\begin{demo}
$dim(q_0) = 0$ \\
$dim(q_0 \vee q_1) \leq 1$ (igualtat $\iff q_1 \neq q_0$)\\
$dim((q_0 \vee q_1) \vee q_2) \leq 2$ (igualtat $\iff q_1 \neq q_2 $ i $q_2 \not\in q_1 \vee q_0$) \\
$\vdots$ \\
$dim((q_0 \vee \dotsc \vee q_{i-1}) \vee q_1) \leq i$
\end{demo}

\begin{defi}
Diem que els punts $q_0, q_1, \dotsc, q_m$ són linealment independents $\iff dim(q_0 \vee  \dotsc \vee q_m) = m$
\end{defi}

\begin{lema}
$q_0, q_1, \dotsc, q_m$ són independents $\iff \forall i >0, q_i \not\in q_0\vee q_1\vee \dotsc\vee q_{i-1} $
\end{lema}

\begin{lema}
Si $q_i=[v_i],\; i= 0, \dotsc, m$, $q_0, q_1, \dotsc, q_m$ independents $\iff v_0, v_1, \dotsc, v_m$ independents.
\end{lema}
\begin{demo}
$q_0, q_1, \dotsc, q_m$ independents $\iff dim(q_0\vee q_1\vee \dotsc\vee q_m)=m$. Si $q_i =[v_i]$, tenim que $\{q_i\} = [<v_i>]$. Aleshores, $q_0 \vee \dotsc\vee q_m = [<v_0>+\dotsc +<v_m>] = [<v_0,\dotsc ,v_m>]$ 
\end{demo}

\begin{prop}
Si $q_0, q_1, \dotsc, q_m$ són independents, tot subconjunt d'aquests punts són també independents.
\end{prop}

\begin{prop}
Sigui $L$ una varietat lineal de dimensió $d$. Siguin $q_0, \dotsc , q_m \in L$ punts independents. Aleshores
\begin{enumerate}
    \item $m\leq d$
    \item $\exists q_{m+1}, \dotsc , q_d \in L$ tal que $q_0, \dotsc , q_m, q_{m+1}, \dotsc , q_d$ són independents (i generen $L$)
\end{enumerate}
\end{prop}

\begin{demo}
Sigui $q_0\vee \dotsc \vee q_m$ espai projectiu de dimensió $m$ tal que $q_0\vee \dotsc \vee q_m \subset L$, amb $dim (L) = d$. Si $m=d$, ja hem acabat. Si $m < d$, $q_0\vee \dotsc \vee q_m \subsetneq L $. Agafem $q_{m+1}\in L \setminus q_0 \vee \dotsc \vee q_m$, de manera que $q_0 \vee \dotsc \vee q_m \vee q_{m+1}$. Podem anar repetint aquest procediment fins que el conjunt de punts generin $L$.
\end{demo}

\begin{defi}
Un símplex $n$-dimensional $\Delta$ de $\mathbb{P}_n$ és un conjunt de $m+1$ punts $q_0, \dotsc , q_m \in \mathbb{P}_n$ independents (amb $m\leq n$). Una cara $d$-dimensional de $\Delta$ és qualsevol unió $p_{i_{0}} \vee \dotsc \vee p_{i_d}$ de $d+1$ punts $d_i$ disjunts. Comunament anomenarem a una cara de dimensió 0 com a vèrtex, una cara de dimensió 1 una aresta...
\end{defi}

\begin{defi}
Suposem donats espais projectius $(\mathbb{P}_n, E, \pi)$ i $(\Bar{\mathbb{P}_m}, \Bar{E}, \Bar{\pi})$ i un isomorfisme $\phi\colon E \to \Bar{E}$ (de manera que $n=m$). Prenem el següent diagrama commutatiu:
%\[
%\begin{tikzcd}
%v \arrow[d, maps to] & E\setminus\{0\} \arrow[r, "\phi_{|E\setminus\{0\}}"] %\arrow[d, "\pi"'] & \bar{E}\setminus \{0\} \arrow[d, "\bar{\pi}"] \\
%p                    & \mathbb{P}_n                                                           & \bar{\mathbb{P}_n}                            \\
                     %& p \arrow[r, maps to]                                                   & {[\phi(v)]}                                  
%\end{tikzcd}
%\]

$\forall p \in \mathbb{P}_n$, agafem $v\mid [v]=p$ i considerem $[\varphi(v)]$. Observem que agafant $v\neq 0$ i per $\varphi$ injectiva $\implies \varphi(v)\neq 0$ i podem considerar $[\varphi(v)]$. Observem també que, si agafem $v' \mid [v']=p$ en lloc de $v$, aleshores $v'=\lambda v \implies \varphi(v')=\lambda \varphi(v) \implies [\varphi(v')]=[\varphi(v)]$. Per tant, $[\varphi(v)]$ està ben definida i no depèn de la tria de $v$.

Tenim l'aplicació induïda per $\varphi$: 
\begin{align*}
    [\varphi]\colon & \mathbb{P}_n \longrightarrow \Bar{\mathbb{P}_n} \\
     & [v] \longmapsto [\varphi(v)]
\end{align*}
\end{defi}

\begin{defi}
$[\varphi]$ és la projectivitat definida per $\varphi$.
\end{defi}

\begin{obs}
$[\varphi] = [\lambda \varphi]$ $\forall \lambda \neq 0$
\end{obs}

\subsubsection{Propietats de les projectivitats}
\begin{enumerate}
    \item Sigui $[\varphi]\colon\mathbb{P}_n\longrightarrow \Bar{\mathbb{P}_n}$ i $[\psi]\colon\Bar{\mathbb{P}_n}\longrightarrow \Bar{\Bar{\mathbb{P}_n}}$, aleshores $[\psi \circ \varphi] = [\psi] \circ [\varphi] $ ($\implies$ composició de projectivitats és projectivitat)
    \begin{demo}
    Afegir un diagrama commutatiu enorme
    \end{demo}
    
    \item $[Id_{E}] = Id_{\mathbb{P}_n}$
    \item $\begin{cases}[\varphi^{-1}] \circ [\varphi] = Id_{\mathbb{P}_n}\\ [\varphi]\circ [\varphi^{-1}] = Id_{\Bar{\mathbb{P}_n}}\end{cases} \implies [\varphi^{-1}]$ inversa de $[\varphi]$. En particular, $[\varphi]$ és bijectiva i la seva inversa és projectivitat
\end{enumerate}

Suposem $L=[F]$ varietat lineal de $\mathbb{P}_n$ i $f=[\varphi]\colon \mathbb{P}_n \longrightarrow \bar{\mathbb{P}_n}$
\begin{enumerate}
    \item $f(L)=[\varphi(F)]$ (en particular $f(L)$ és varietat lineal de dimensió $= dim(L)$.
    \begin{demo}
    \begin{itemize}
        \item[$\boxed{\subseteq}$]: $p \in f(L) \implies p = f(q)$ per a algun $q\in L \implies $ si $q=[v]$, $v\in F \implies \varphi(v)\in \varphi(F)$ i $p=[\varphi(v)] \implies p\in [\varphi(F)]$
        \item[$\boxed{\supseteq}$]: $p\in [\varphi(F)]$, $p=[w]$ per a alguna $w\in \varphi(F)$. Sigui $v\mid \varphi(v)=w$. Aleshores $p=[\varphi(v)] = [\varphi]([v]) \in f(L)$
    \end{itemize}
    \end{demo}
    
    \item $f_{\mid L}\colon L \longrightarrow f(L)$ és projectivitat (és la projectivitat definida per $[\varphi_{\mid L}]$.
    \begin{demo}
    $\forall p=[v] \in L$, $f_{\mid L}(p) = [\varphi(v)] = [\varphi_{\mid L}(v)]$
    \end{demo}
    
    \item $L_1\subset L_2 \implies f(L_1)\subset f(L_2)$
    \item $f(L_1\cap L_2) = f(L_1)\cap f(L_2)$
    \item $f(L_1\vee L_2)=f(L_1) \vee f(L_2)$
    \begin{demo}
    \begin{itemize}
        \item[$\boxed{\subseteq}$]: Sigui $p\in f(L_1 \vee L_2) \implies p=f(q)$ per a algun $q\in[v_1 + v_2]$, amb $v_1\in F_1$, $v_2\in F_2$. $p=[\varphi(v_1 + v_2)] = [\varphi(v_1) + \varphi(v_2)] \in [\varphi(F_1)+ \varphi(F_2)] = f(L_1)+f(L_2)$
        \item[$\boxed{\supseteq}$]: $p\in f(l_1)\vee f(L_2) \implies p = [w_1 + w_2]$, amb $w_1 \in \varphi(F_1)$ amb $v_1 \in F_1$ i $w_2 \in \varphi(F_2)$ amb $v_2\in F_2$. Aleshores $p=[\varphi(v_1)+\varphi(v_2)]=[\varphi(v_1+v_2)]=f([v_1+v_2])$, $[v_1+v_2]\in L_1 \vee L_2$
    \end{itemize}
    \end{demo}
    \item $q_0,\dotsc , q_m \in \mathbb{R}_n$ independents $\iff f(p_0), \dotsc , f(p_m)$ independents
    \begin{demo}
    $q_0,\dotsc , q_m$ independents $\iff dim(q_0\vee \dotsc \vee q_m)=m \iff dim(f(q_0\vee \dotsc \vee q_m))=m \iff fim(f(q_0)\vee \dotsc \vee f(q_m)) = m \iff  f(q_0),\dotsc , f(q_m)$ independents
    \end{demo}
\end{enumerate}

\begin{defi}
Anomenem quadrilàter a la figura formada per quatre rectes de $\mathbb{P}_2$, totes concurrents. Anomenem vèrtexs a les interseccions de costats disjunts i diem que dos vèrtex són oposats sii no comparteixen cap costat. Anomenem diagonal a la unió de vèrtexs oposats.
\end{defi}

\begin{thm}
Les diagonals no concorrents són els costats d'un triangle anomenat triangle triagonal.
\end{thm}

\begin{defi}
Anomenem quatrivèrtex a un conjunt de quatre punts (vèrtex) no alineats. Els costats són les unions de vèrtexs diferents i diem que els costats són oposats si no comparteixen vèrtexs. Els punts són diagonals si són interseccions de costats oposats.
\end{defi}

\begin{thm}[Desargues]
Siguin $ABC$ i $A'B'C'$ triangles de $\mathbb{P}_2$ tals que cap vèrtex d'un triangle és a un costat de l'añtre triangle. Si $AA'$, $BB'$, $CC'$ concurrents, aleshores si $\begin{cases} a= BC, b = AC, c=AB \\ a'=B'C', b'?A'C', c'=A'B'\end{cases} \implies a\cap a'$, $b\cap b'$ i $c\cap c'$ són punts alineats.
\end{thm}

\begin{thm}[Pappus]
Siguin $A$, $B$, $C$ tres punts alineats i $A'$, $B'$ i $C'$ tres altres punts alineats. Aleshores $AB'\cap A'B$, $AC'\cap A'C$ i $BC'\cap B'C$ estàn també alineats.
\end{thm}


\section{Referències projectives}
\begin{defi}
Una referència projectiva a $\mathbb{P}_n$ és un conjunt ordenat de $n+2$ punts $\Delta = \{p_0,\dotsc, p_n; A\}$ tal que:
\begin{enumerate}
    \item $p_0,\dotsc, p_n$ independents ($p_0,\dotsc,p_n$ símplex $=$ símplex de la referència)
    \item $\forall i = 0, \dotsc, n$, $A\not\in p_0 \vee \dotsc \vee \hat{p_i} \vee \dotsc \vee p_n$.
\end{enumerate}
\end{defi}

\begin{defi}
$e_0, \dotsc ,e_n\in E$ s'anomena base adaptada a $\Delta = \{p_0,\dotsc, p_n; A\}$ sii els vectors són representants dels vèrtexs i la seva suma representa el punt unitat.
\end{defi}

\begin{lema}
Tota referència té una base adaptada.
\end{lema}
\begin{demo}
Agafem $v_i$ amb $i=0,\dotsc n$ tal que $[v_i]=p_i \implies v_0, \dotsc, v_n$ base de $E$. Sigui $v\mid [v]=A$, $v=\lambda_0v_0, \dotsc, \lambda_nv_n$. Necessàriament $\lambda_i\neq 0$, $i=0,\dotsc,n$. Si tinguéssim $\lambda_i=0$, $v=\lambda_0v_0 + \dotsc + \widehat{\lambda_iv_i} + \dotsc + \lambda_nv_n$, aleshores $A\in p_0\vee \dotsc \vee \hat{p_i}\vee \dotsc \vee p_n$, que co compleix l'apartat (2) de la definició de referència projectiva. Agafant $e_i = \lambda_iv_i$, tenim que $e_i\neq 0$ i $e_0,\dotsc, e_n$ formen base adaptada.
\end{demo}


\begin{lema}
Si $e_0, \dotsc, e_n$ i $v_0, \dotsc, v_n$ són bases adaptades de $\Delta$, aleshores $\exists \rho\in K \mid e_i=\rho v_i$; $\forall i$.
\end{lema}
\begin{demo}
$\begin{cases}p_i=[e_i]=[v_i]\implies \exists \rho\in K \mid e_i=\rho_i v_i \\ A=[\sum e_i]=[\sum v_i]\implies \exists \rho\in K \mid \sum e_i=\rho \sum v_i\end{cases} \implies \sum \rho_i v_i = \rho\sum v_i \implies \sum(\rho_i - \rho)v_i= 0 \implies \rho_i-\rho=0\; \forall i$.
\end{demo}

\section{Coordenades}
Agafem $\forall p\in \mathbb{P}_n$ i $\Delta$ referència.
\begin{defi}
$x_0, \dotsc, x_n \in K$ són coordenades de $p$ relatives a $\Delta \iff \exists$ base adaptada a $\delta$ $e_0, \dotsc, e_n \mid p=[ x_0e_0, \dotsc, x_ne_n]$ (necessàriament $(x_0, \dotsc, x_n)\neq (0,\dotsc ,0)$.  
\end{defi}
 Amb aquesta definició tenim les següents propietats:
 \begin{itemize}
     \item $\forall p\in\mathbb{P}_n$ tenen coordenades i si $x_0, \dotsc, x_n$ i $y_0, \dotsc, y_n$ són coordenades de $p$, aleshores $\exists \delta\in K$ de manera que $x_i=\delta y_i\; \forall i$.
     \begin{demo}
     \begin{enumerate}
         \item \textbf{Tot punt té coordenades:} Agafem $p=[v]$ i $e_0, \dotsc, e_n$ base adaptada. Aleshores $v=x_0e_0, \dotsc, x_ne_n$ i $(x_0, \dotsc, x_n)$ seràn les coordenades de $p$.
         \item Sigui $p=[x_0e_0, \dotsc, x_ne_n] = [y_0v_0, \dotsc, y_nv_n]$ amb $e_0, \dotsc, e_n$ i $v_0, \dotsc, v_n$ bases adaptades. Aleshores $(x_0e_0, \dotsc, x_ne_n) = \lambda(y_0v_0, \dotsc, y_nv_n)$ i $(x_0e_0, \dotsc, x_ne_n)=(x_0\rho v_0, \dotsc, x_n\rho v_n)$ pel lema anterior, de manera que $(\lambda y_0-\rho x_0)v_0 + \dotsc + (\lambda y_n-\rho x_n)v_n \implies \lambda y_i-\rho x_i=0\implies \delta=\frac{\lambda}{\rho}$.
     \end{enumerate}
     \end{demo}
     
     \item $\forall (x_0, \dotsc, x_n)\in K^{n+1}\setminus\{0\}$, $\exists ! p$ amb coordenades $(x_0, \dotsc, x_n)$
     \begin{demo}
     Mirem primer l'existència: $\exists e_0, \dotsc, e_n$ base adaptada tal que $[x_0e_0, \dotsc, x_ne_n]$ té coordenades $x_0, \dotsc, x_n$. Comprovem la unicitat: Siguin $p,q$ dos punts de tals que $p=[x_0e_0, \dotsc, x_ne_n]$ i $q=[x_0v_0, \dotsc, x_nv_n]$. Pel lema anterior tenim que $e_i=\rho v_i$.
     \end{demo}
 \end{itemize}
 
 \begin{lema}
 Si $p$ té coordenades $x_0, \dotsc, x_n$, aleshores $\forall$ base adaptada $v_0, \dotsc, v_n$, $p=[x_0v_0, \dotsc, x_nv_n]$.
 \end{lema}
 \begin{demo}
 $p=[x_0e_0, \dotsc, x_ne_n]$ per a alguna base $e_i$
 \end{demo}
 
 \section{Incidència amb coordenades}
 Sigui una referència $\delta=(p_0, \dotsc, p_n, A)$ de $\mathbb{P}_n$. Aleshores, $\forall e_0, \dotsc, e_n$ base adaptada de $\delta$, $p=[x_0, \dotsc, x_n] \iff p=[x_0e_0 +\dotsc + x_ne_n]$.
 
 \subsection{Independència de punts}
 Punts $q_i = [b_i^0, \dotsc, b_i^n]$, amb $i=0,\dotsc, d$ són independents $\iff rg \begin{pmatrix} b_0^0 & \cdots & b_d^0 \\ \vdots & & \vdots \\ b_0^n & \cdots & b_d^n  \end{pmatrix} = d+1$
 
 \subsection{Representació de varietats lineals}
 \subsubsection{Equacions paramètriques}
 Sigui $L=[F]$ varietat lineal de dimensió $d$, i punts $q_0, \dotsc, q_d$ independents en $L$ (que generen $L$). $q_i = [b_i^0, \dotsc, b_i^d] \iff v_i = (b_i^0, \dotsc, b_i^d)$ són representants dels $q_i$ i formen base de $F$. Aleshores $q\in L \iff q = [v]$, amb $v\in F \iff \exists \lambda_0, \dotsc, \lambda_d \mid v= \lambda_0 v_0 + \dotsc + \lambda_d v_d$. És a dir, $q=[x_0, \dotsc x_d] \in L \iff \exists \lambda_0, \dotsc, \lambda_d$ tal que 
 \[\begin{pmatrix} x_0 \\ \vdots \\ x_d \end{pmatrix} = \lambda_0 \begin{pmatrix}b_0^0 \\ \vdots \\ b_0^n\end{pmatrix} + \cdots + \lambda _d\begin{pmatrix}b_d^0 \\ \vdots \\ b_d^n \end{pmatrix}\]
 
 D'altra banda, per a punts $[x_0, \dotsc, x_d]$ donats, per a qualsevol igualtat d'aquestes amb columnes independents generarien una varietat lineal de dimensió $d$.
 
 \subsubsection{Representació implícita de L}
 Tenim que $q=[v]\in L \iff v\in F$. Suposem $q=[x_0, \dotsc, x_n]$ i agafem $v=(x_0, \dotsc, x_n)$. Aleshores $\exists (n-1)-(d-1)=n-d$ equacions homogènies independents. Aeshores $v\in F \iff (x_0, \dotsc, x_n)$ és solució de les equacions.
 
 \[q\in L \iff v= x_0e_0 + \dotsc + x_ne_n \in F \iff rg\begin{pmatrix} b_0^0 & \cdots & b_0^n \\ \vdots & & \vdots \\ b_n^0 & \cdots & b_n^n \\ x_0 & \cdots & x_n \end{pmatrix} \leq d+1\]
 
 \subsection{Càlcul de la unió}
 Sigui $L$ un espai projectiu i $q_0, \dotsc, q_d$ punts independents en $L$ que el generen, i sigui $L'$ un altre espai projectiu i $q_0', \dotsc, q_d'$ un conjunt de punts independents en $L'$ que el generen. Aleshores $L\vee L' = q_0 \vee \dotsc \vee q_d \vee q_0' \vee \dotsc \vee q_d'$. Extreiem de $q_0, \dotsc, q_d, q_0', \dotsc, q_d'$ un sistema maximal de equacions i independents i agafem les coordenades com a columnes del sistema.
 
 \subsection{Càlcul de la intersecció}
 Per a calcular la intersecció de dues varietats, ajuntem les equacions de les varietats.
 \subsection{Inclusió}
 $L\subset L' \iff L\vee L' = L' \land L\cap L' = L$
 
 \section{Canvis de referència}
 Siguin $\Delta = (p_0, \dotsc, p_n, A)$ i $\Omega = (q_0, \dotsc, q_n, B)$ dues referències de $\mathbb{P}_n$. Tenim $p=[x_0, \dotsc, x_n]_\Delta = [y_0, \dotsc, y_n]_\Omega$. Com relacionem $x$'s i $y$'s? Suposem els punts de la referència $\Omega$ en la referència $\Delta$:
 \begin{align}
     q_0 = [\bar{a}_0^0, \dotsc, \bar{a}^n_0]_\Delta = [a_0^0, \dotsc, a_0^n] \\
     \vdots \\
     q_m = [\bar{a}_n^0, \dotsc, \bar{a}^n_m]_\Delta = [a_n^0, \dotsc, a_n^n] \\
     B=[a^0, \dotsc, a^n]_\Delta
 \end{align}
  i els escribim de manera que $a^i=\sum_{j=0}^n a_j^i$. Això és possible perquè les equacions que surten de forçar això formen un sistema de cramer. Els representants $v_i$ dels punts adaptats $[a_i^0, \dotsc, a_i^n]$ formen una base adaptada a $B$:
  \begin{align}
      v_0 = a_0^0e_0 + \dotsc + a_0^ne_n \\
      \vdots \\
      v_n = a_n^0e_0 + \dotsc + a_n^ne_n \\
  \end{align}
  
Aleshores el punt p
\[p= [\sum_{i=0}^ny_iv_i] = [\sum_i y_i \sum_j a_i^je_j] = [\sum_j(\sum_i y_ia_i^j)e_j]\], o de manera matricial

\[\begin{pmatrix} x_0 \\ \vdots \\ x_n \end{pmatrix} = \rho \begin{pmatrix} a_0^0 & \cdots & a_n^0 \\ \vdots & & \vdots \\ a_0^n & \cdots & a_n^n \end{pmatrix}\begin{pmatrix}y_0 \\ \vdots \\ y_n\end{pmatrix}\]

\section{Coordenades absolutes}
A $\mathbb{P}_1$ amb la referència $\Delta = (p_0, p_1, A)$ considerem l'aplicació
 \begin{align*}
     \mathbb{P}_1 & \longrightarrow k\cup{\infty} \\
     p=[x_0, x_1] &\longmapsto \begin{cases} \frac{x_0}{x_1} & x_1 \neq 0 \\ \infty & x_1=0 (\iff p=[1,0]) \end{cases}
\end{align*}

\section{Raó doble}
Suposem punts $q_1, q_2, q_3, q_4 \in \mathbb{P}_1$, com a mínim 3 diferents. Agafem una referència $\Delta = (q_1, q_2, A)$. Denotem $q_i = [x_i, y_i]$. 
Considerem 
\[\rho = \frac{    \begin{vmatrix} x_3 & y_3 \\ x_1 & y_1 \end{vmatrix}}{\begin{vmatrix} x_3 & y_3 \\ x_2 & y_2 \end{vmatrix}}\colon \frac{    \begin{vmatrix} x_4 & y_4 \\ x_1 & y_1 \end{vmatrix}}{\begin{vmatrix} x_4 & y_4 \\ x_2 & y_2 \end{vmatrix}} \in k\cup \{\infty\} \]

\begin{lema}
$\rho$ no varia si canviem de coordenades per a cada $q_i$ o conviem de referència.
\begin{demo}
Sigui $q_i=[x_i, y_i]=[\lambda_ix_i, \lambda_iy_i]$. Aleshores 
\[\begin{vmatrix} \lambda_i x_i & \lambda_iy_i \\ \lambda_jx_j & \lambda_jy_j \end{vmatrix} = \lambda_i\lambda_j \begin{vmatrix} x_i & y_i \\ x_j & y_j \end{vmatrix} \implies \frac{\lambda_3\lambda_1\begin{vmatrix} & \end{vmatrix}}{\lambda_3\lambda_2\begin{vmatrix} & \end{vmatrix}}\colon \frac{\lambda_4\lambda_1\begin{vmatrix} & \end{vmatrix}}{\lambda_4\lambda_2\begin{vmatrix} & \end{vmatrix}}\].

Si canviem de referència:\\
Agafem la referència $\Omega$ tal que $q_i=[\bar{x_i}, \bar{y_i}]_\Omega$. Aleshores \[M\begin{bmatrix}x_i & x_j \\ y_i & y_j\end{bmatrix} = \begin{bmatrix}\bar{x_i} & \bar{x_j} \\ \bar{y_i} & \bar{y_j}\end{bmatrix}\]
de manera que 
\[detM\begin{vmatrix}x_i & x_j \\ y_i & y_j\end{vmatrix} = \begin{vmatrix}\bar{x_i} & \bar{x_j} \\ \bar{y_i} & \bar{y_j}\end{vmatrix}\]
\end{demo}
\end{lema}
\end{document}
