import graph3;
import contour;
import palette;
//settings.outformat="html";
size(12cm,0);

currentprojection=orthographic(1,-2,1);
currentlight=(1,-1,0.5);

//Definim la funció z=(x^2-y^2)/(2x^2+y^2) 
real f(pair z) {
    if(z.x==0 && z.y==0) 
        return -1;
    else
        return (z.x^2-z.y^2)/(2z.x^2+z.y^2);
}

//I els intervals sobre els que la volem calcular:
pair a=(-1,-1); 
pair b=(1,1);

//Creem la superfície definida per f i els límits marcats
surface s = surface(f,a,b,nx=50);
draw(s, mean(palette(s.map(zpart),Rainbow())),black);



xaxis3(XY()*"$x$",OutTicks(XY()*Label));
yaxis3(XY()*"$y$",InTicks(YX()*Label));
zaxis3("$z$",OutTicks);
