import graph3;
import contour;
import grid3;

size(12cm,0);

currentprojection=orthographic(1,-2,1);
currentlight=(1,-1,0.5);

//Definim la funció x^2+y^2 
real f(pair z) {return abs(z)^2;}

//I els intervals sobre els que la volem calcular:
pair a=(-1,-1); 
pair b=(1,1);

//Creem la superfície definida per f i els límits marcats
surface s = surface(f,a,b,Spline);
draw(s, gray+opacity(0.5));

//Dibuixem el contorn de f en z=0.5
draw(lift(f,contour(f,(-1,-1),(1,1),new real[] {0.5})));

//Definim una paràbola d'R2 i la dibuixem 
real g(real x) { return -x^2; }
path q = graph(g, -1, 1, operator..);
draw(path3(q));


//Parametritxem la corba donada per la imatge de f sobre els punts de g
real A(real t) {return t;}
real B(real t) {return -t^2;}
real C(real t) {return t^2+t^4;}

path3 p=graph(A,B,C,-1,1,operator ..);

draw(p,2bp+.8red);


xaxis3(XY()*"$x$",OutTicks(XY()*Label));
yaxis3(XY()*"$y$",InTicks(YX()*Label));
zaxis3("$z$",OutTicks);
