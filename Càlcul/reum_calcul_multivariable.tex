\documentclass[utf8, catalan]{article}
\usepackage[utf8]{inputenc}
\usepackage[catalan]{babel} %install texlive-lang-spanish
\usepackage{listings}
\usepackage{amstext}
\usepackage{authblk}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{color} 
\usepackage{graphicx}
\usepackage{float}
\usepackage{enumerate}
\usepackage{parskip}
\usepackage{faktor} %found in texlive-science
\usepackage{ stmaryrd }
\usepackage{ mathrsfs }
\usepackage{tikz}
\usepackage[nottoc]{tocbibind}

\usetikzlibrary{external}
\tikzexternalize[prefix=figures/]

\newtheoremstyle{break}% name
{}%         Space above, empty = `usual value'
{}%         Space below
{\itshape}% Body font
{}%         Indent amount (empty = no indent, \parindent = para indent)
{\bfseries}% Thm head font
{.}%        Punctuation after thm head
{\newline}% Space after thm head: \newline = linebreak
{}%
\theoremstyle{break}



\DeclarePairedDelimiter\abs{\lvert}{\rvert}%


\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}
%

\newtheorem{defi}{Definició}

\title{Resum Càlcul diferencial multivariable}
\author{Aniol}
\date{}
\begin{document}

\maketitle


\section{Límits de funcions en diverses variables}
En càlcul diferencial d'una variable diem que una funció $f(x)$ té limit $l$ en el punt a  ($\lim_{x\to a}f(x) = l$) si els límits laterals en $a$ coincideixen i tenen valor $l$, és  a dir, si  $\lim_{x\to a^-}(x) = \lim_{x\to a^+}f(x)$. En una variable, només ens podem aproximar al punt des de dues direccions: l'esquerra del punt i la dreta del punt. Per exemple, a la funció de la figura següent ens podem aproximar al $0$ per l'esquerra ( seguint la recta $y=0$) o bé per la dreta (seguint la recta $y=1$):

En diverses variables, això canvia. A un punt d'una superfície, ens hi podem aproximar mitjançant infinits camins. Suposem que tenim una funció $f\colon \mathbb{R}^2 \to \mathbb{R}$  i volem trobar el $\lim_{(x,y)\to(a,b)}f(x,y)$ . Pel punt $(a,b)\in\mathbb{R}^2$ hi passen infinits camins: rectes, corbes, funcions estranyes... Per tal que la funció $f$ sigui contínua, cal que $\lim_{(x,y)\to(a,b)}f(x,y)$ amb $(x,y)$ restringit a cada un d'aquests conjunts sigui el mateix. Vegem, per exemple el cas de la funció $f(x,y)=x^2+y^2$: 

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{images/limit0.pdf}
\end{figure}

Ens podem aproximar a $(0,0)$ per sobre la paràbola $y=x^2$, $y=-x^2$, les rectes de la forma $y=nx$... I si mirem les imatges de tots aquests conjunts, veiem que 
\[\lim_{\substack{(x,y)\to(0,0)\\ y=x^2}}f(x,y) = \lim_{\substack{(x,y)\to(0,0)\\ y=-x^2}}f(x,y) = \lim_{\substack{(x,y)\to(0,0)\\ y=x}}f(x,y)=\dotsc\]

En canvi, si agafem la funció $f(x,y)=\frac{x^2-y^2}{2x^2+y^2}$, tenim que $\lim_{\substack{(x,y)\to(0,0)\\ y=0}}f(x,y) =\frac{1}{2}$ i que  $\lim_{\substack{(x,y)\to(0,0)\\ x=0}}f(x,y) =-1$:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{images/limit.pdf}
\end{figure}


De manera més general: sigui $D\subset \mathbb{R}^n$, $f\colon D \to \mathbb{R}$, $E\subset D$ i $a\in \mathbb{R}^n$ un punt d'acumulació de $E$ (i per tant punt d'acumulació de $D$), el límit de $f$ sobre el conjunt $E$ és simplement $\lim_{x\to a}f_{|E}(x)$ (sovint escrit com a $\lim_{\substack{x\to a \\ x\in E}}f(x)$). I és evident que \[\exists \lim_{x\to a }f(x)=l \implies \exists \lim_{\substack{x\to a \\ x\in E}}f(x)=l\]


Com passa sovint, demostrar l'existència del límit és complicat: cal veure que el límit existeix per a tots els camins que passen per aquest punt. En canvi, demostrar que no existeix és molt més senzill, només cal trobar un camí sobre el qual el límit no existeixi.

Per demostrar la no existència del límit, hi ha certes tècniques que ens poden ajudar:
\begin{itemize}
    \item Calculem primer els límits sobre les rectes $x=a$ i $y=b$, que seran les més senzilles. Si els límits no coincideixen, ja hem demostrat la no existència. Si coincideixen, ja tenim un candidat a límit. Només que trobem un límit diferent d'aquests, ja haurem acabat.
    \item Si ens trobem una funció racional i el denominador s'anul·la en una corba que passa pel punt que estudiem, és molt probable que el límit no existeixi. És a dir, en fraccions fem el límit sobre corbes que anul·lin el denominador en el punt que estudiem.
    \item Busquem un camí tal que en fer la substitució el grau del numerador i el del denominador s'igualin.
\end{itemize}

Per provar que el límit sí que existeix, el més fàcil sol ser utilitzar la regla del Sandwich, concretament la versió amb el valor absolut, que diu així: 

\fbox{
\parbox{\textwidth}{\centering Si $|f(x)-b| \leq \varphi(x)$ i $\lim_{x\to a}\varphi(x)=0$, aleshores $\lim_{x\to a}f(x)=b$}
}

Observem que el valor absolut d'aquesta versió de la regla del Sandwich ens és mol útil per dues raons: 
\begin{itemize}
	\item El valor absolut està acotat inferiorment per $0$, de manera que no ens cal buscar una cota inferior.
	\item El valor absolut de la funció ens permet buscar una fita superior de manera molt més senzilla, ja que no ens hem de preocupar dels possibles valors negatius (i els corresponents canvis de sentit de la desigualtat) quan treballem amb desigualtats.
	
\end{itemize}
\section{Continuitat}
Diem que una funció $f\colon D \to \mathbb{R}^m$ és contínua en $a\in D$ si 
\[\forall \epsilon > 0\;\exists\delta >0  \text{ tal que } \forall x \in D\,\|x-a\|<\delta \implies \|f(x)-f(a)\|<\epsilon\] Això és equivalent a dir que $f$ és contínua en $a$ si $a$ és un punt aïllat de $D$ o si $a$ és punt d'acumulació de $D$ i $\lim_{x\to a} f(x) = f(a)$. 

Diem que $f$ és contínua en $D\iff \forall a\in D$, $f$ és contínua.

Exemples de funcions contínues són:
\begin{itemize}
	\item Funcions polinòmiques en $ \mathbb{R}^n$
	\item Funcions racionals en $\mathbb{R}^n$ (tals que el denominador no s'anul·la)
	\item Suma de funcions contínues
	\item Producte d'una funció contínua i un escalar o una altra funció contínua
	\item Composició de funcions contínues
	\item Projeccions de $\mathbb{R}^n \to \mathbb{R}$
	\item Funció vectorial amb totes les components contínues
\end{itemize}


\section{Diferenciabilitat}
\subsection{Derivades parcials i direccionals}
En funcions d'una sola variable, la derivada de la funció en un punt ens indicava la pendent de la recta tangent a la funció per aquell punt. Ara bé, en funcions de diverses variables, la imatge de la funció ja no és una corba, sinó que és una superfície. En aquest cas, no podem dir que la derivada de la funció és el pendent de la recta tangent a la superfície per un punt, ja que ara hi ha infinites rectes tangents a la superfície per un punt (de fet tenim un hiperplà tangent a la superfície). Ara bé, si ens limitem a la recta tangent en una certa direcció, sí que podem calcular-ne el pendent. I això és exactament el que fan les derivades direccionals. Vegem-ne la definició formal:
\begin{defi}
	Sigui $U\subset\mathbb{R}^n$ un obert, $f\colon U \to \mathbb{R}$ una funció. $a\in U$
	un punt i $v\in \mathbb{R}^n$ un vector unitari. Aleshores, la derivada direccional de $f$ en $a$ segons la direcció $v$ és
	\[D_vf(a)=\lim_{t\to 0}\frac{f(a+tv)-f(a)}{t}\]
	quan aquest límit existeix i és finit.
\end{defi}
Quan fem la derivada direccional d'una funció de dues variables sobre un punt $a$, el que realment fem és  buscar el pla perpendicular al pla $xy$ que passa pel punt $a$ en la direcció determinada i estudiar la intersecció d'aquest pla amb la superfície. Això és l'equivalent a fer la derivada de la funció $g(t)=f(a+tv)$ en $t=0$. Com que $f$ està definida en un entorn de $a$, $g$ ho està en un entorn de $0$.

Això ens és útil quan sabem el punt i la direcció a priori, però no és tan senzill quan volem alguna formula més general. Ara bé, si estudiem la derivada en les direccions dels vectors de la base canònica del conjunt de sortida, podrem tractar la resta de variables com a constants. Llavors les interseccions seran funcions d'una sola variable, que sabem com tractar. Aquestes derivades especials s'anomenen derivades parcials.

\begin{defi}
	Si $v=e_j=(0,\dotsc,\overset{\overset{j}{\smile}}{1},\dotsc,0)$, la derivada direccional $D_{e_j}f(a)$ l'anomenarem derivada parcial de $f$ en $a$ respecte $x_j$ i l'escriurem com $\frac{\partial f}{\partial x_j}(a)$ o $f_{x_j}(a)$. Aleshores
	\[\frac{\partial f}{\partial x_j}(a)=\lim_{t\to 0}\frac{f(a+te_j)-f(a)}{t}=\lim_{x_j\to \infty}\frac{f(a_1,\dotsc,x_j,\dotsc,a_n)-f(a)}{x_j-a_j}\]
\end{defi} 

%Afegir algun exemple

\subsection{Funcions escalars diferenciables}
Sigui $I\subset \mathbb{R}$ un interval no trivial, $f\colon I\to \mathbb{R}$, $a\in I$.
Diem que $F$ és derivable en $a$ si 
\begin{equation*}
\begin{split}
&\exists (f'(a)=)\lim_{x\to a}\frac{f(x)-f(a)}{x-a}\in \mathbb{R} \iff \\
& \iff \exists m (=f'(a))\in \mathbb{R} \text{ tal que } \lim_{x \to a}\frac{f(x)-f(a)}{x-a}-m = 0 \iff \\
& \iff \exists m\in \mathbb{R} \text{ tal que } \lim_{x\to a}\frac{f(x)-f(a)-m(x-a)}{x-a}=0
\end{split}
\end{equation*}
\[ \exists (f'(a)=)\lim_{x\to a}\frac{f(x)-f(a)}{x-a}\in \mathbb{R} \iff \exists m (=f'(a))\in \mathbb{R} \text{ tal que } \lim_{x \to a}\frac{f(x)-f(a)}{x-a}-m = 0 \iff \exists m\in \mathbb{R}\] tal que $\lim_{x\to a}\frac{f(x)-f(a)-m(x-a)}{x-a}=0 \iff \abs{\lim_{x\to a}\frac{f(x)-f(a)-m(x-a)}{x-a}=0} \iff \lim_{x\to a}\frac{f(x)-f(a)-m(x-a)}{\abs{x-a}}=0 \iff \exists L\colon \mathbb{R}\to \mathbb{R}$ lineal tal que $\lim_{x\to a}\frac{f(x)-f(a)-L(x-a)}{\abs{x-a}}=0 \;(L(x) = f'(a)x)$.

\end{document}
